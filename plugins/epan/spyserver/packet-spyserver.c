/* packet-spyserver.c
 * Routines for spyserver packet dissection
 * Copyright 2019 Hansi Reiser <dl9rdz@darc.de>
 *
 * Wireshark - Network traffic analyzer
 * By Gerald Combs <gerald@wireshark.org>
 * Copyright 1998 Gerald Combs
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */


#include "config.h"

#include <stdio.h>
#include <epan/packet.h>
#include <epan/prefs.h>
#include <epan/dissectors/packet-tcp.h>

#define SPYSERVER_PORTS "5555-5559"

static int proto_spyserver = -1;

// Server to client
static int hf_spyserver_protocol_id = -1;
static int hf_spyserver_message_type = -1;
static int hf_spyserver_stream_type = -1;
static int hf_spyserver_sequence_number = -1;
static int hf_spyserver_body_size = -1;
// .. DeviceInfo
static int hf_spyserver_di_device_type = -1;
static int hf_spyserver_di_device_serial = -1;
static int hf_spyserver_di_max_smpl_rate = -1;
static int hf_spyserver_di_max_bandwidth = -1;
static int hf_spyserver_di_decstagecnt = -1;
static int hf_spyserver_di_gainstagecnt = -1;
static int hf_spyserver_di_maxgainindex = -1;
static int hf_spyserver_di_minfreq = -1;
static int hf_spyserver_di_maxfreq = -1;
static int hf_spyserver_di_resolution = -1;
static int hf_spyserver_di_miniqdec = -1;
static int hf_spyserver_di_forcediq = -1;
// .. ClientSync
static int hf_spyserver_cs_can_control = -1;
static int hf_spyserver_cs_gain = -1;
static int hf_spyserver_cs_dev_center_freq = -1;
static int hf_spyserver_cs_iq_center_freq = -1;
static int hf_spyserver_cs_fft_center_freq = -1;
static int hf_spyserver_cs_min_iq_center = -1;
static int hf_spyserver_cs_max_iq_center = -1;
static int hf_spyserver_cs_min_fft_center = -1;
static int hf_spyserver_cs_max_fft_center = -1;

// Client to server
static int hf_spyserver_command_type = -1;
// static int hf_spyserver_body_size = -1;
static int hf_spyserver_client_id = -1;
// .. set command
static int hf_spyserver_set_type = -1;
static int hf_spyserver_set_value = -1;

static gint ett_spyserver = -1;

void proto_register_spyserver(void);

struct spyserver_flow_data {
   int version;
};

static const value_string devicetype_names[] _U_ = {
  { 1, "Airspy One" },
  { 2, "Airspy HF+" },
  { 3, "RTLSDR" },
  { 0, "Invalid device" },
  { -1, NULL }
};
static const value_string commandtype_names[] _U_ = {
  { 0, "CMD_HELLO" },
  { 1, "CMD_GET_SETTING" },
  { 2, "CMD_SET_SETTING" },
  { 3, "CMD_PING" }
};
static const value_string settingtype_names[] _U_ = {
  { 0, "SETTING_STREAMING_MODE" },
  { 1, "SETTING_STREAMING_ENABLED" },
  { 2, "SETTING_GAIN" },

  { 100, "SETTING_IQ_FORMAT" },
  { 101, "SETTING_IQ_FREQUENCY" },
  { 102, "SETTING_IQ_DECIMATION" },
  { 103, "SETTING_IQ_DIGITAL_GAIN" },
 
  { 200, "SETTING_FFT_FORMAT" },
  { 201, "SETTING_FFT_FREQUENCY" },
  { 202, "SETTING_FFT_DECIMATION" },
  { 203, "SETTING_FFT_DB_OFFSET" },
  { 204, "SETTING_FFT_DB_RANGE" },
  { 205, "SETTING_FFT_DISPLAY_PIXELS" }
};
static const value_string streammode_names[] _U_ = {
  { 0, "STREAM_TYPE_STATUS" },
  { 1, "STREAM_TYPE_IQ_ONLY" },
  { 2, "STREAM_TYPE_AF_ONLY" },
  { 4, "STREAM_TYPE_FFT_ONLY" },
  { 5, "STREAM_MODE_FFT_IQ" },
  { 6, "STREAM_MODE_FFT_AF" }
};
static const value_string streamformat_names[] _U_ = {
  { 0, "STREAM_FORMAT_INVALID" },
  { 1, "STREAM_FORMAT_UINT8" },
  { 2, "STREAM_FORMAT_INT16" },
  { 3, "STREAM_FORMAT_INT24" },
  { 4, "STREAM_FORMAT_FLOAT" },
  { 5, "STREAM_FORMAT_DINT4" }
};
static const value_string messagetype_names[] _U_ = {
  { 0, "MSG_TYPE_DEVICE_INFO" },
  { 1, "MSG_TYPE_CLIENT_SYNC" },
  { 2, "MSG_TYPE_PONG" },
  { 3, "MSG_TYPE_READ_SETTING" },

  { 100, "MSG_TYPE_UINT8_IQ" },
  { 101, "MSG_TYPE_INT16_IQ" },
  { 102, "MSG_TYPE_INT24_IQ" },
  { 103, "MSG_TYPE_FLOAT_IQ" },

  { 200, "MSG_TYPE_UINT8_AF" },
  { 201, "MSG_TYPE_INT16_AF" },
  { 202, "MSG_TYPE_INT24_AF" },
  { 203, "MSG_TYPE_FLOAT_AF" },

  { 300, "MSG_TYPE_DINT4_FFT" },
  { 301, "MSG_TYPE_UINT8_FFT" }
};

#define SERVER_FRAME_HEADER_LEN (5*4)
#define CLIENT_FRAME_HEADER_LEN (2*4)

int searchsync=0;
static int maxsize=4096;  

// SpyServer messages, server to client
static int
dissect_spyserver_srvmsg(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data _U_, guint32 offset)
{
   guint32 len = tvb_captured_length(tvb);
   /* Check if still in sync (packet loss or frame size > maxsize), resynchronize if needed */
   while(offset+3<len) {
      int val = tvb_get_guint32(tvb, offset, ENC_LITTLE_ENDIAN);
      if(val==0x020006a4) break;
      offset++;
   }
   if(offset+3>=len) { /* not found */
      col_append_fstr(pinfo->cinfo, COL_INFO, "(additional SpyServer data); ");
      return len;
   }
   if(len-offset<20) {
      pinfo->desegment_offset = offset;
      pinfo->desegment_len = DESEGMENT_ONE_MORE_SEGMENT;
      return -1;
   }
   guint16 build = tvb_get_guint16(tvb, offset+0, ENC_LITTLE_ENDIAN);
   guint8 minor = tvb_get_guint8(tvb, offset+2);
   guint8 major = tvb_get_guint8(tvb, offset+3);
   guint32 msg_seqnr = tvb_get_guint32(tvb, offset+12, ENC_LITTLE_ENDIAN);
   guint32 msg_size = tvb_get_guint32(tvb, offset+16, ENC_LITTLE_ENDIAN);
   guint32 real_size = msg_size;
  
   if(msg_size>(guint32)maxsize) { msg_size=(guint32)maxsize; searchsync=1; }

   /* check if sufficient length, request more data if necessary */
   if(offset+20+msg_size>len) {
      pinfo->desegment_offset = offset;
      pinfo->desegment_len = DESEGMENT_ONE_MORE_SEGMENT;
      return -1;
   }
   //if(msg_size==maxsize) { msg_size = len - 20 - offset; }

   col_set_str(pinfo->cinfo, COL_PROTOCOL, "SpyServer");
   proto_item *ti = proto_tree_add_item(tree, proto_spyserver, tvb, offset+0, -1, ENC_NA);

   proto_tree *spyserver_tree = proto_item_add_subtree(ti, ett_spyserver);

   proto_item *protoid = proto_tree_add_item(spyserver_tree, hf_spyserver_protocol_id, tvb, offset+0, 4, ENC_LITTLE_ENDIAN);
   proto_item_append_text(protoid, " - protocol version %d.%d build %d", major, minor, build);

   proto_item *mtype = proto_tree_add_item(spyserver_tree, hf_spyserver_message_type, tvb, offset+4, 2, ENC_LITTLE_ENDIAN);
   guint16 subtype = tvb_get_guint16(tvb, offset+6, ENC_LITTLE_ENDIAN);
   if(subtype != 0) {
      proto_item_append_text(mtype, " - aux %d (%x) ", subtype, subtype);
   }

   proto_tree_add_item(spyserver_tree, hf_spyserver_stream_type, tvb, offset+8, 4, ENC_LITTLE_ENDIAN);
   proto_tree_add_item(spyserver_tree, hf_spyserver_sequence_number, tvb, offset+12, 4, ENC_LITTLE_ENDIAN);
   proto_tree_add_item(spyserver_tree, hf_spyserver_body_size, tvb, offset+16, 4, ENC_LITTLE_ENDIAN);

   guint type = tvb_get_guint32(tvb, offset+4, ENC_LITTLE_ENDIAN) & 0xffff;
   switch(type) {
   case 0: // MSG_TYPE_DEVICE_INFO
      {
      int pos=20+offset;
      proto_tree_add_item(spyserver_tree, hf_spyserver_di_device_type, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_di_device_serial, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_di_max_smpl_rate, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_di_max_bandwidth, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_di_decstagecnt, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_di_gainstagecnt, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_di_maxgainindex, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_di_minfreq, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_di_maxfreq, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_di_resolution, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_di_miniqdec, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_di_forcediq, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      col_append_fstr(pinfo->cinfo, COL_INFO, "DeviceInfo");//, info_type, info_serial);
      }
      break;
   case 1: // MSG_TYPE_CLIENT_SYNC
      {
      int pos=20+offset;
      proto_tree_add_item(spyserver_tree, hf_spyserver_cs_can_control, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_cs_gain, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_cs_dev_center_freq, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_cs_iq_center_freq, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_cs_fft_center_freq, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_cs_min_iq_center, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_cs_max_iq_center, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_cs_min_fft_center, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      proto_tree_add_item(spyserver_tree, hf_spyserver_cs_max_fft_center, tvb, pos, 4, ENC_LITTLE_ENDIAN); pos+=4;
      col_append_fstr(pinfo->cinfo, COL_INFO, "ClientSync");
      }
      break;
   default:
      break;
   }
   if(type>=100&&type<200) {
      col_append_fstr(pinfo->cinfo, COL_INFO, "IQ(#%d): %dbyte; ", msg_seqnr, real_size);
   } else if (type>=300) {
      col_append_fstr(pinfo->cinfo, COL_INFO, "FFT: %dbyte; ", real_size);
   }
   return offset+20+msg_size;
}

static guint
get_spyserver_clnt_message_len(packet_info *pinfo _U_, tvbuff_t *tvb, int offset, void *data _U_)
{
   return (guint)tvb_get_guint32(tvb, offset+4, ENC_LITTLE_ENDIAN) + 8;
}

static int
dissect_spyserver_clntmsg(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data _U_)
{
   col_set_str(pinfo->cinfo, COL_PROTOCOL, "SpyServer");

   proto_item *ti = proto_tree_add_item(tree, proto_spyserver, tvb, 0, -1, ENC_NA);

   proto_tree *spyserver_tree = proto_item_add_subtree(ti, ett_spyserver);

   proto_tree_add_item(spyserver_tree, hf_spyserver_command_type, tvb, 0, 4, ENC_LITTLE_ENDIAN);
   proto_tree_add_item(spyserver_tree, hf_spyserver_body_size, tvb, 4, 4, ENC_LITTLE_ENDIAN);
   guint type = tvb_get_guint32(tvb, 0, ENC_LITTLE_ENDIAN);
   switch(type) {
   case 0: // HELLO
      {
      guint16 build = tvb_get_guint16(tvb, 8, ENC_LITTLE_ENDIAN);
      guint8 minor = tvb_get_guint8(tvb, 10);
      guint8 major = tvb_get_guint8(tvb, 11);
      proto_item *protoid = proto_tree_add_item(spyserver_tree, hf_spyserver_protocol_id, tvb, 8, 4, ENC_LITTLE_ENDIAN);
      proto_item_append_text(protoid, " - protocol version %d.%d build %d", major, minor, build);
      guint32 len = tvb_get_guint32(tvb, 4, ENC_LITTLE_ENDIAN);
      proto_item *version _U_= proto_tree_add_item(spyserver_tree, hf_spyserver_client_id, tvb, 12, len-4, ENC_LITTLE_ENDIAN);
      col_append_fstr(pinfo->cinfo, COL_INFO, "HELLO(%d.%d.%d); ", major, minor, build);
      }
      break;
   case 1: // GET_SETTING
      break;
   case 2: // SET_SETTING
      {
      guint32 len = tvb_get_guint32(tvb, 4, ENC_LITTLE_ENDIAN);
      proto_tree_add_item(spyserver_tree, hf_spyserver_set_type, tvb, 8, 4, ENC_LITTLE_ENDIAN);
      proto_tree_add_item(spyserver_tree, hf_spyserver_set_value, tvb, 12, len-4, ENC_LITTLE_ENDIAN);
      gint32 settype = tvb_get_gint32(tvb, 8, ENC_LITTLE_ENDIAN);
      gint32 setvalue = tvb_get_gint32(tvb, 12, ENC_LITTLE_ENDIAN);
      col_append_fstr(pinfo->cinfo, COL_INFO, "SET(%d=%d); ", settype, setvalue);
      }
      break;
   case 3: // PING
      break;
   }
   return tvb_captured_length(tvb);
}

static int
dissect_spyserver_srv(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data)
{
/*
    tcp_dissect_pdus(tvb, pinfo, tree, TRUE, SERVER_FRAME_HEADER_LEN,
                     get_spyserver_srv_message_len, dissect_spyserver_srvmsg, data);
    return tvb_captured_length(tvb);
*/
   guint32 offset = 0;
   guint32 available_bytes = tvb_captured_length(tvb);
   guint32 totallen = available_bytes;
   while(available_bytes>=20) {
      if(offset>totallen) {
	//printf("should not happen: offset %d > totallen %d (avail=%d)\n", offset, totallen, available_bytes);
        available_bytes = 0;
        break;
      }
      int len = dissect_spyserver_srvmsg(tvb, pinfo, tree, data, offset);
      if(len>0) { available_bytes -= (len-offset); offset = len; continue; }
      else { return len; }
   }
   return totallen-available_bytes;
}

static int
dissect_spyserver_clnt(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data)
{
   tcp_dissect_pdus(tvb, pinfo, tree, TRUE, CLIENT_FRAME_HEADER_LEN,
                     get_spyserver_clnt_message_len, dissect_spyserver_clntmsg, data);
   return tvb_captured_length(tvb);
}

static int
dissect_spyserver(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data)
{
   conversation_t *conversation;
   int ret;
   int is_client;
   struct spyserver_flow_data *flowdata;
   conversation = find_or_create_conversation(pinfo);
   flowdata = (struct spyserver_flow_data *)conversation_get_proto_data(conversation, proto_spyserver);
   if(!flowdata) {
      flowdata = (struct spyserver_flow_data *)wmem_alloc0(wmem_file_scope(), sizeof(struct spyserver_flow_data));
      conversation_add_proto_data(conversation, proto_spyserver, flowdata);
   }
   col_clear(pinfo->cinfo, COL_INFO);
   is_client = pinfo->destport < pinfo->srcport;  // using some heuristic....
   if(is_client) { // client->server
      ret=dissect_spyserver_clnt(tvb, pinfo, tree, data);
   } else { // server->client
      ret=dissect_spyserver_srv(tvb, pinfo, tree, data);
   }
   col_prepend_fstr(pinfo->cinfo, COL_INFO, "%s: ", is_client ? "Client" : "Server" );
   return ret;
}


void
proto_register_spyserver(void){
   static hf_register_info hf[] = {
      { &hf_spyserver_client_id,
         { "Client ID", "spyserver.clientid",
           FT_STRING, BASE_NONE,
           NULL, 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_protocol_id,
         { "Protocol ID", "spyserver.protoid",
           FT_UINT32, BASE_HEX,
           NULL, 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_message_type,
         { "Message Type", "spyserver.msgtype",
           FT_UINT16, BASE_DEC,
           VALS(messagetype_names), 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_stream_type,
         { "Stream Type", "spyserver.streamtype",
           FT_UINT32, BASE_DEC,
           VALS(streammode_names), 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_sequence_number,
         { "Sequence Number", "spyserver.seqnr",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_body_size,
         { "Body Size", "spyserver.bodysize",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_command_type,
         { "Command type", "spyserver.cmdtype",
           FT_UINT32, BASE_DEC,
           VALS(commandtype_names), 0x0,
           NULL, HFILL }
      },
      // Device info
      { &hf_spyserver_di_device_type,
         { "Device type", "spyserver.di.devicetype",
           FT_UINT32, BASE_DEC,
           VALS(devicetype_names), 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_di_device_serial,
         { "Device serial", "spyserver.di.serial",
           FT_UINT32, BASE_HEX,
           NULL, 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_di_max_smpl_rate,
         { "Maximum sample rate", "spyserver.di.maxrate",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_di_max_bandwidth,
         { "Maximum bandwidth", "spyserver.di.bandwidth",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_di_decstagecnt,
         { "Decimation stage count", "spyserver.di.decstagecnt",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_di_gainstagecnt,
         { "Gain stage count", "spyserver.di.gainstagecnt",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_di_maxgainindex,
         { "Maximum gain index", "spyserver.di.maxgainindex",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_di_minfreq,
         { "Minimum frequency", "spyserver.di.minfreq",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_di_maxfreq,
         { "Maximum frequency", "spyserver.di.maxfreq",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_di_resolution,
         { "Resolution", "spyserver.di.resolution",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_di_miniqdec,
         { "Minimum IQ decimation", "spyserver.di.miniqdec",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
      },
      { &hf_spyserver_di_forcediq,
         { "Forced IQ format", "spyserver.di.forcediq",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
      },
      // Client sync
     { &hf_spyserver_cs_can_control,
         { "Can control", "spyserver.di.cancontrol",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
     },
     { &hf_spyserver_cs_gain,
         { "Gain", "spyserver.di.gain",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
     },
     { &hf_spyserver_cs_dev_center_freq,
         { "Device center frequency", "spyserver.di.devcenterfreq",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
     },
     { &hf_spyserver_cs_iq_center_freq,
         { "IQ center frequency", "spyserver.di.iqcenterfreq",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
     },
     { &hf_spyserver_cs_fft_center_freq,
         { "FFT center frequency", "spyserver.di.fftcenterfreq",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
     },
     { &hf_spyserver_cs_min_iq_center,
         { "Minimum IQ center frequency", "spyserver.di.miniqcenter",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
     },
     { &hf_spyserver_cs_max_iq_center,
         { "Maximum IQ center frequency", "spyserver.di.maxiqcenter",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
     },
     { &hf_spyserver_cs_min_fft_center,
         { "Minimum FFT center frequency", "spyserver.di.minfftcenter",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
     },
     { &hf_spyserver_cs_max_fft_center,
         { "Maximum FFT center frequency", "spyserver.di.maxfftcenter",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
     },
     // client: set
     { &hf_spyserver_set_type,
         { "Setting type", "spyserver.di.settype",
           FT_UINT32, BASE_DEC,
           VALS(settingtype_names), 0x0,
           NULL, HFILL }
     },
     { &hf_spyserver_set_value,
         { "Setting value", "spyserver.di.setvalue",
           FT_UINT32, BASE_DEC,
           NULL, 0x0,
           NULL, HFILL }
     },
     
   };
   static gint *ett[] = {
      &ett_spyserver
   };
   proto_spyserver = proto_register_protocol("SpyServer protocol", "SpyServer", "spyserver");

   proto_register_field_array(proto_spyserver, hf, array_length(hf));
   proto_register_subtree_array(ett, array_length(ett));

   module_t *spyserver_prefs = prefs_register_protocol(proto_spyserver, NULL);
   prefs_register_uint_preference(spyserver_prefs, "maxsize", "MaxSize",
       "Maximum size of data packets to be collected by the dissector", 10, &maxsize);
}


void
proto_reg_handoff_spyserver(void) {
   dissector_handle_t spyserver_handle;

   spyserver_handle = create_dissector_handle(dissect_spyserver, proto_spyserver);
   dissector_add_uint_range_with_preference("tcp.port", SPYSERVER_PORTS, spyserver_handle);
}


/*
 * Editor modelines  -  http://www.wireshark.org/tools/modelines.html
 *
 * Local Variables:
 * c-basic-offset: 3
 * tab-width: 8
 * indent-tabs-mode: nil
 * End:
 *
 * ex: set shiftwidth=3 tabstop=8 expandtab:
 * :indentSize=3:tabSize=8:noTabs=true:
 */
